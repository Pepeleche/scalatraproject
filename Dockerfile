FROM tomcat

ADD target/first_project-1.0-SNAPSHOT.jar /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]